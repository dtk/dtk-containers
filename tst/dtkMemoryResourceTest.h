// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkContainersTests.h"

class dtkMemoryResourceTestCase : public QObject
{
    Q_OBJECT

public:
     dtkMemoryResourceTestCase(void);
    ~dtkMemoryResourceTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testBasic(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    class dtkMemoryResourceTestCasePrivate *d;
};

//
// dtkMemoryResourceTest.h ends here
