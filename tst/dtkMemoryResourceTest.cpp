// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMemoryResourceTest.h"

#include <dtkMemoryResource.h>
#include <dtkArray.h>

#include <QtCore>

// ///////////////////////////////////////////////////////////////////
// dtkMemoryResourceTestCasePrivate
// ///////////////////////////////////////////////////////////////////

class dtkMemoryResourceTestCasePrivate
{
public:
    dtkMemoryResourceTestCasePrivate(void) : mr(nullptr) {;}
    ~dtkMemoryResourceTestCasePrivate(void) {;}

public:
    dtk::MemoryResource *mr;

public:
    bool checkAlignment(void *ptr, std::size_t alignment);
};

// ///////////////////////////////////////////////////////////////////

bool dtkMemoryResourceTestCasePrivate::checkAlignment(void *ptr, std::size_t alignment)
{
    //qDebug() << "test" << (((std::uintptr_t)ptr) & (alignment - 1));

    //return ((reinterpret_cast<std::uintptr_t>(ptr) % alignment) == 0);
    return ((((std::uintptr_t)ptr) & (alignment - 1)) == 0);
}

template <typename T> dtk::Array<T> copyOfResizedArray(std::size_t size, dtk::Array<T>& a)
{
    a.resize(2 * size);

    return a;
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkMemoryResourceTestCase::dtkMemoryResourceTestCase(void) : QObject(), d(new dtkMemoryResourceTestCasePrivate)
{
    d->mr = new dtk::MemoryResourceAlign;
}

dtkMemoryResourceTestCase::~dtkMemoryResourceTestCase(void)
{
    delete d->mr;
    delete d;
}

void dtkMemoryResourceTestCase::initTestCase(void)
{

}

void dtkMemoryResourceTestCase::init(void)
{

}

void dtkMemoryResourceTestCase::cleanup(void)
{

}

void dtkMemoryResourceTestCase::cleanupTestCase(void)
{

}

void dtkMemoryResourceTestCase::testBasic(void)
{

    {
        double *b = nullptr;
        std::size_t b_size = 7563249;
        std::size_t b_alignment = std::alignment_of<double>::value;

        b = static_cast<double *>(d->mr->allocate(b_size, b_alignment));

        QVERIFY(d->checkAlignment(b, b_alignment));
        qDebug() << d->checkAlignment(b, 16) << sizeof(std::max_align_t) << std::alignment_of<long double>::value;

        b = static_cast<double *>(d->mr->deallocate(b, sizeof(double)));
        QVERIFY(!b);
    }

    {
        double *b = nullptr;
        std::size_t b_size = 7563249;
        std::size_t b_alignment = 128;

        b = static_cast<double *>(d->mr->allocate(b_size, b_alignment));

        QVERIFY(d->checkAlignment(b, b_alignment));
        qDebug() << d->checkAlignment(b, 8);

        b = static_cast<double *>(d->mr->deallocate(b, b_alignment));
        QVERIFY(!b);
    }

    {
        std::size_t N = 7684;
        double pi = 3.14159;
        dtk::Array<double> a(N, pi);
        QCOMPARE(N, a.size());
        QCOMPARE(N, a.capacity());

        N = 10000;
        a.resize(N);
        QCOMPARE(N, a.size());
        QCOMPARE(N, a.capacity());

        std::size_t M = 7;
        a.resize(M);
        QCOMPARE(M, a.size());
        QCOMPARE(N, a.capacity());

        for(std::size_t i = 0; i < a.size(); ++i) {
            QCOMPARE(pi, a[i]);
        }

        dtk::ConstArrayView<double> cv = a;
        for(std::size_t i = 0; i < a.size(); ++i) {
            QCOMPARE(pi, cv[i]);
        }

        dtk::ArrayView<double> vv = a;
        vv[1] = 2.;
        QCOMPARE(2.,  a[1]);
        QCOMPARE(2., cv[1]);

        dtk::ConstArrayView<double> cvv = vv;
        vv[1] = pi;
        for(std::size_t i = 0; i < a.size(); ++i) {
            QCOMPARE(pi, cvv[i]);
        }

        std::cout << a << std::endl;


        dtk::Array<double> b = {0., 1, 2, 3, 4., 5., 6.};
        for(std::size_t i = 0; i < b.size(); ++i) {
            QCOMPARE(i*1., b[i]);
        }
        std::cout << "b = " << b << std::endl;


        dtk::Array<double> c = copyOfResizedArray(7, b);
        std::cout << "c = " << c << std::endl;

        a = std::move(copyOfResizedArray(6, b));
        std::cout << "a = " << a << std::endl;

        //a.resize(15);
        //a.resize(12);
        //std::initializer_list<double> list = { 5.4, 29.0 };
        auto it = a.cbegin() + 4;
        std::cout << "a.capacity = " << a.capacity() << std::endl;
        //a.insert(it, list.begin(), list.end());
        a.emplace(a.cend(), 1385865);
        a.emplace(a.cbegin(), 1385865);
        a.emplace(a.cend(), 1385865);
        a.emplace(a.cend(), 1385865);

        std::cout << "a = " << a << std::endl;

        it = a.cbegin() + 4;
        a.erase(it, it + 5);
        std::cout << "a = " << a << std::endl;

    }

}


DTKCONTAINERSTEST_MAIN_NOGUI(dtkMemoryResourceTest, dtkMemoryResourceTestCase)

//
// dtkMemoryResourceTest.cpp ends here
