// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkContainers.h"

namespace dtk {

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

    template < typename T > inline BufferManager<T>::BufferManager(void) : BufferManager<T>(containers::memory_resource::policy())
{

}

template < typename T > inline BufferManager<T>::BufferManager(const std::string& mem_resource_policy) : mem_resource(containers::memory_resource::pluginFactory().create(mem_resource_policy))
{

}

template < typename T > inline BufferManager<T>::BufferManager(MemoryResourcePointer mem_r) : mem_resource(mem_r)
{

}

template < typename T > inline BufferManager<T>::BufferManager(BufferManager&& o) : mem_resource(o.mem_resource)
{
    o.mem_resource.reset(new dtk::MemoryResourceNull);
}

template < typename T > template < typename U > inline BufferManager<T>::BufferManager(const BufferManager<U>& o) : mem_resource(o.mem_resource)
{

}

template < typename T > inline BufferManager<T>::~BufferManager(void)
{

}

template < typename T > inline BufferManager<T>& BufferManager<T>::operator = (const BufferManager& o)
{
    mem_resource = o.mem_resource;

    return *this;
}

template < typename T > template < typename U > inline BufferManager<T>& BufferManager<T>::operator = (const BufferManager<U>& o)
{
    mem_resource = o.mem_resource;
}

template < typename T > inline T *BufferManager<T>::allocate(std::size_t n)
{
    //return static_cast<T *>(mem_resource->allocate(n * sizeof(T), std::alignment_of<T>::value));
    return static_cast<T *>(mem_resource->allocate(n * sizeof(T), 32UL));
}

template < typename T > inline void BufferManager<T>::deallocate(T *ptr, std::size_t n)
{
    mem_resource->deallocate(ptr, n * sizeof(T), std::alignment_of<T>::value);
}

template < typename T > inline MemoryResourcePointer BufferManager<T>::resource(void) const
{
    return mem_resource;
}

template < typename T > template < typename U, typename... Args > inline void BufferManager<T>::construct(U *ptr, Args&&... args)
{
    ::new(static_cast<void *>(ptr)) U(std::forward<Args>(args)...);
}

template < typename T > template < typename U > inline void BufferManager<T>::destroy(U *ptr)
{
    ptr->~U();
}

template < typename T > template < typename U, typename... Args > inline typename std::enable_if< std::is_fundamental<U>::value>::type BufferManager<T>::initBuffer(U *from, U *to, Args&&... args)
{
    std::uninitialized_fill(from, to, U(std::forward<Args>(args)...));
}

template < typename T > template < typename U, typename... Args > inline typename std::enable_if<!std::is_fundamental<U>::value>::type BufferManager<T>::initBuffer(U *from, U *to, Args&&... args)
{
    std::uninitialized_fill(from, to, U(std::forward<Args>(args)...));
}

template < typename T > template < typename U > inline typename std::enable_if< std::is_fundamental<U>::value>::type BufferManager<T>::copyBuffer(const U *s_from, const U *s_to, U *t_from)
{
    std::copy(s_from, s_to, t_from);
}

template < typename T > template < typename U > inline typename std::enable_if<!std::is_fundamental<U>::value>::type BufferManager<T>::copyBuffer(const U *s_from, const U *s_to, U *t_from)
{
    std::copy(s_from, s_to, t_from);
}

template < typename T > template < typename U > inline typename std::enable_if< std::is_fundamental<U>::value>::type BufferManager<T>::clearBuffer(U *from, U *to)
{
    //std::fill(from, to, U());
}

template < typename T > template < typename U > inline typename std::enable_if<!std::is_fundamental<U>::value>::type BufferManager<T>::clearBuffer(U *from, U *to)
{
    if (mem_resource->identifier() != "MemoryResourceNull") {
        for(; from != to; ++from) {
            this->destroy(from);
        }
    }
}

}; // dtk namespace ends here

//
// dtkBufferManager.tpp ends here
