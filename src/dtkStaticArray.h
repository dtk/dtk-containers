// Version: $Id: 6cd5079363cdc21c0a184dc9e51a6f47f67d7872 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <cstring>
#include <cstddef>

#include <type_traits>
#include <iterator>

// ///////////////////////////////////////////////////////////////////
// dtkStaticArrayData interface
// ///////////////////////////////////////////////////////////////////

template < typename T, std::size_t Alignment, std::size_t Size, bool = std::is_pod<T>::value > struct dtkStaticArrayData
{
    T _data[Size];

    constexpr const T *data(void) const { return _data; }
                    T *data(void)       { return _data; }
};

template < typename T, std::size_t Alignment > struct dtkStaticArrayData <T, Alignment, 0, false>
{
    constexpr const T *data(void) const { return nullptr; }
                    T *data(void)       { return nullptr; }
};

template < typename T, std::size_t Alignment, std::size_t Size > struct dtkStaticArrayData<T, Alignment, Size, true>
{
    alignas(Alignment) T _data[Size];

    constexpr const T *data(void) const { return _data; }
                    T *data(void)       { return _data; }
};

template < typename T, std::size_t Alignment > struct dtkStaticArrayData <T, Alignment, 0, true>
{
    constexpr const T *data(void) const { return nullptr; }
                    T *data(void)       { return nullptr; }
};

// ///////////////////////////////////////////////////////////////////
// dtkStaticArray interface
// ///////////////////////////////////////////////////////////////////

template < typename T, std::size_t Size, std::size_t Alignment = std::alignment_of<T>::value > class dtkStaticArray
{
public:
    dtkStaticArrayData<T, Alignment, Size> d;

public:
    typedef T                  value_type;
    typedef value_type*        pointer;
    typedef const value_type*  const_pointer;
    typedef value_type&        reference;
    typedef const value_type&  const_reference;
    typedef std::ptrdiff_t     difference_type;
    typedef std::size_t      size_type;

public:
    typedef value_type*                                         iterator;
    typedef const value_type*                             const_iterator;
    typedef iterator                                            Iterator;
    typedef const_iterator                                 ConstIterator;
    typedef std::reverse_iterator<iterator>	            reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

public:
    void swap(dtkStaticArray& rhs) { std::swap_ranges(begin(), end(), rhs.begin()); }

public:
    constexpr bool         empty(void) const { return !Size; }
    constexpr std::size_t   size(void) const { return  Size; }
    constexpr std::size_t  count(void) const { return  Size; }

public:
    void fill(const T& value) { std::fill_n(begin(), Size, value); }

    void setAt(std::size_t index, const T& value) { data()[index] = value; }

public:
    const T&          at (std::size_t index) const { return *(data() + index); }
    const T& operator [] (std::size_t index) const { return *(data() + index); }
          T& operator [] (std::size_t index)       { return *(data() + index); }

public:
                    reference front(void)       { return *(data()); }
    constexpr const_reference front(void) const { return *(data()); }
                    reference  back(void)       { return *(data() + Size - 1); }
    constexpr const_reference  back(void) const { return *(data() + Size - 1); }

public:
                    T& first(void)       { return front(); }
    constexpr const T& first(void) const { return front(); }
                    T&  last(void)       { return  back(); }
    constexpr const T&  last(void) const { return  back(); }

public:
                    iterator  begin(void)       { return       iterator(data()); }
    constexpr const_iterator  begin(void) const { return const_iterator(data()); }
    constexpr const_iterator cbegin(void) const { return const_iterator(data()); }

                    iterator  end(void)       { return       iterator(data() + Size); }
    constexpr const_iterator  end(void) const { return const_iterator(data() + Size); }
    constexpr const_iterator cend(void) const { return const_iterator(data() + Size); }

                    reverse_iterator  rbegin(void)       { return       reverse_iterator(end()); }
    constexpr const_reverse_iterator  rbegin(void) const { return const_reverse_iterator(end()); }
    constexpr const_reverse_iterator crbegin(void) const { return const_reverse_iterator(end()); }

                    reverse_iterator  rend(void)       { return       reverse_iterator(begin()); }
    constexpr const_reverse_iterator  rend(void) const { return const_reverse_iterator(begin()); }
    constexpr const_reverse_iterator crend(void) const { return const_reverse_iterator(begin()); }

public:
    constexpr const T *data(void) const { return d.data(); }
                    T *data(void)       { return d.data(); }

    constexpr const T *constData(void) const { return d.data(); }
};

// ///////////////////////////////////////////////////////////////////
// Compararison operators
// ///////////////////////////////////////////////////////////////////

template < typename T, std::size_t Size> inline bool operator == (const dtkStaticArray<T, Size>& lhs, const dtkStaticArray<T, Size>& rhs)
{
    return std::equal(lhs.begin(), lhs.end(), rhs.begin());
}

template < typename T, std::size_t Size> inline bool operator != (const dtkStaticArray<T, Size>& lhs, const dtkStaticArray<T, Size>& rhs)
{
    return !(lhs == rhs);
}

template < typename T, std::size_t Size> inline bool operator < (const dtkStaticArray<T, Size>& lhs, const dtkStaticArray<T, Size>& rhs)
{
    return std::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
}

template < typename T, std::size_t Size> inline bool operator > (const dtkStaticArray<T, Size>& lhs, const dtkStaticArray<T, Size>& rhs)
{
    return rhs < lhs;
}

template < typename T, std::size_t Size> inline bool operator <= (const dtkStaticArray<T, Size>& lhs, const dtkStaticArray<T, Size>& rhs)
{
    return !(lhs > rhs);
}

template < typename T, std::size_t Size> inline bool operator >= (const dtkStaticArray<T, Size>& lhs, const dtkStaticArray<T, Size>& rhs)
{
    return !(lhs < rhs);
}


//
// dtkStaticArray.h ends here
