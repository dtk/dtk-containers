// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <vector>
#include <unordered_map>
#include <string>
#include <iostream>

namespace dtk {

// /////////////////////////////////////////////////////////////////
// dtkContainersPluginFactory
// /////////////////////////////////////////////////////////////////

template < typename T > class ContainersPluginFactory
{
public:
    virtual ~ContainersPluginFactory(void) {}

public:
    typedef T *(*creator) ();

public:
    void record(const std::string& key, creator func);

public:
    T *create(const std::string& key) const;

public:
    std::vector<std::string> keys(void) const;

private:
    std::unordered_map<std::string, creator> creators;
};

// /////////////////////////////////////////////////////////////////

template <typename T> inline void ContainersPluginFactory<T>::record(const std::string& key, creator func)
{
    // if (this->creators.find(key) == this->creators.end()) {

    //     std::cout << "ContainersPluginFactory<T>::record(): Factory already contains key " << key << ". Nothing is done." << std::endl;
    //     return;
    // }

    this->creators.insert({key, func});
}

template <typename T> inline T *ContainersPluginFactory<T>::create(const std::string& key) const
{
    auto it = this->creators.find(key);

    if (it == this->creators.end())
        return nullptr;

    return (it->second)();
}

template <typename T> std::vector<std::string> ContainersPluginFactory<T>::keys(void) const
{
    std::vector<std::string> key_list(this->creators.size());
    std::size_t i = 0;
    for (const auto &pair : this->creators) {
        key_list[i++] = pair.first;
    }
    return key_list;
}

}; // end of dtk namespace

//
// dtkContainersPluginFactory.h ends here
