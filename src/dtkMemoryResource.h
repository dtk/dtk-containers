// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <cstddef>
#include <memory>

#include "dtkContainersExport.h"

#include "dtkContainersPluginFactory.h"

namespace dtk {

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class DTKCONTAINERS_EXPORT MemoryResource
{
public:
             MemoryResource(void) {;}
    virtual ~MemoryResource(void) {;}

public:
    virtual void *allocate(std::size_t bytes, std::size_t alignment = alignof(std::max_align_t)) = 0;
    virtual void *deallocate(void *ptr, std::size_t bytes, std::size_t alignment = alignof(std::max_align_t)) = 0;

public:
    virtual bool equal(const MemoryResource& other) const = 0;

public:
    virtual std::string identifier(void) const = 0;
};

typedef std::shared_ptr<MemoryResource> MemoryResourcePointer;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class DTKCONTAINERS_EXPORT MemoryResourceFactory : public ContainersPluginFactory<MemoryResource>
{
public:
     MemoryResourceFactory(void);
    ~MemoryResourceFactory(void);

public:
    void setPolicy(const std::string& policy_name);
    const std::string& policy(void) const;

private:
    std::string m_policy;
};

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class DTKCONTAINERS_EXPORT MemoryResourceAlign : public MemoryResource
{
public:
    void *allocate(std::size_t bytes, std::size_t alignment = alignof(std::max_align_t));
    void *deallocate(void *ptr, std::size_t bytes, std::size_t alignment = alignof(std::max_align_t));

public:
    bool equal(const MemoryResource& other) const;

public:
    std::string identifier(void) const;
};

inline MemoryResource *MemoryResourceAlignCreator(void)
{
    return new MemoryResourceAlign;
}

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class MemoryResourceAlignHandler
{
public:
    static void *allocate(std::size_t bytes, std::size_t alignment);
    static void *deallocate(void *ptr, std::size_t bytes, std::size_t alignment);

public:
    bool equal(const MemoryResource& other) const;
};

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class DTKCONTAINERS_EXPORT MemoryResourceNull : public MemoryResource
{
public:
    void *allocate(std::size_t, std::size_t = alignof(std::max_align_t)) { return nullptr; }
    void *deallocate(void *, std::size_t, std::size_t = alignof(std::max_align_t)) { return nullptr; }

public:
    bool equal(const MemoryResource& other) const { return (this == &other); }

public:
    std::string identifier(void) const { return "MemoryResourceNull"; };
};

inline MemoryResource *MemoryResourceNullCreator(void)
{
    return new MemoryResourceNull;
}

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class DTKCONTAINERS_EXPORT MemoryResourceMallocAndFree : public MemoryResource
{
public:
    void *allocate(std::size_t bytes, std::size_t alignment = alignof(std::max_align_t));
    void *deallocate(void *ptr, std::size_t bytes, std::size_t alignment = alignof(std::max_align_t));

public:
    bool equal(const MemoryResource& other) const;

public:
    std::string identifier(void) const;
};

inline MemoryResource *MemoryResourceMallocAndFreeCreator(void)
{
    return new MemoryResourceMallocAndFree;
}

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class DTKCONTAINERS_EXPORT MemoryResourceNewAndDelete : public MemoryResource
{
public:
    void *allocate(std::size_t bytes, std::size_t alignment = alignof(std::max_align_t));
    void *deallocate(void *ptr, std::size_t bytes, std::size_t alignment = alignof(std::max_align_t));

public:
    bool equal(const MemoryResource& other) const;

public:
    std::string identifier(void) const;
};

inline MemoryResource *MemoryResourceNewAndDeleteCreator(void)
{
    return new MemoryResourceNewAndDelete;
}

}; // dtk namespace ends here

//
// dtkMemoryResource.h ends here
