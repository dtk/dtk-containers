// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

/*!
  \class dtk::ArrayBase
  \internal
  \inmodule dtkContainer

  \brief The dtk::ArrayBase is documented as dtk::ConstArrayView class.

*/

/*!
  \class dtk::ArrayMid
  \internal
  \inmodule dtkContainer

  \brief The dtk::ArrayMid is documented as dtk::ArrayView class.

*/

/*!
  \class dtk::ArrayFinal
  \internal
  \inmodule dtkContainer

  \brief The dtk::ArrayFinal is documented as dtk::Array class.

*/

//
// dtkArray.cpp ends here
