// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


/*!
  \class dtkArray
  \inmodule dtkContainer
  \brief The dtkArray class is an alias for \c { dtk::ArrayFinal<T> } class.

  dtkArray is a dynamically allocatable array of arbitrary element types.
  The elements are stored linearly in memory.
*/


/*! \typedef dtkArray::iterator

    Synonym of \c {dtkArrayIterator<T>}.
*/


/*! \typedef dtkArray::const_iterator

    Synonym of \c {dtkArrayConstIterator<T>}
*/


/*! \typedef dtkArray::reverse_iterator

    Synonym of \c {std::reverse_iterator<iterator>}
*/


/*! \typedef dtkArray::const_reverse_iterator

    Synonym of \c {std::reverse_iterator<const_iterator>}
*/


/*! \typedef dtkArray::value_type

    Synonym of \c {T}
*/


/*! \typedef dtkArray::pointer

    Synonym of \c {T*}
*/


/*! \typedef dtkArray::const_pointer

    Synonym of \c {const T*}
*/


/*! \typedef dtkArray::reference

    Synonym of \c {T&}
*/


/*! \typedef dtkArray::const_reference

    Synonym of \c {const T&}
*/


/*! \typedef dtkArray::difference_type

    Synonym of \c {std::ptrdiff_t}
*/


/*! \typedef dtkArray::size_type

    Synonym of \c {std::size_t}
*/


/*! \typedef dtkArray::buffer_manager_type

    Synonym of \c {dtkBufferManager<T>}
*/


/*! \typedef dtkArray::ConstView

    Synonym of \c {dtkConstArrayView<T>}
*/


/*! \typedef dtkArray::View

    Synonym of \c {dtkArrayView<T>}
*/


/*! \typedef dtkArray::NoView

    Synonym of \c {dtkArray<T>}
*/


/*! \fn dtkArray::dtkArray(void)

    Default constructor. Constructs an empty container.

    \code
    #include <dtkArray>
    #include <string>
    #include <iostream>

    int main()
    {
       // c++11 initializer list syntax:
       dtkArray<std::string> words1 {"the", "frogurt", "is", "also", "cursed"};
       std::cout << "words1: " << words1 << '\n';

       // words2 == words1
       dtkArray<std::string> words2(words1.begin(), words1.end());
       std::cout << "words2: " << words2 << '\n';

       // words3 == words1
       dtkArray<std::string> words3(words1);
       std::cout << "words3: " << words3 << '\n';

       // words4 is {"Mo", "Mo", "Mo", "Mo", "Mo"}
       dtkArray<std::string> words4(5, "Mo");
       std::cout << "words4: " << words4 << '\n';
    }

    The above code produces:
    \code
    words1: Array of size 5
    [the, frogurt, is, also, cursed]
    words2: Array of size 5
    [the, frogurt, is, also, cursed]
    words3: Array of size 5
    [the, frogurt, is, also, cursed]
    words4: Array of size 5
    [Mo, Mo, Mo, Mo, Mo]
    \endcode

    \sa resize
*/

/*! \fn dtkArray::dtkArray(const dtkBufferManager<T>& manager)

    Constructs a new empty container with a user supplied
    dtkBufferManager \a manager.

*/

/*! \fn dtkArray::dtkArray(size_type count, const dtkBufferManager<T>& manager = dtkBufferManager<T>())

    Constructs the container with \a count default-inserted instances
    of \c{T}. No copies are made. User can supply a custom
    dtkBufferManager<T> \a manager.

*/

/*! \fn dtkArray::dtkArray(size_type count, const T& value, const dtkBufferManager<T>& manager = dtkBufferManager<T>())

    Constructs the container with \a count copies of elements with
    value \a value. User can supply a custom dtkBufferManager<T> \a
    manager.
*/

/*! \fn dtkArray::dtkArray(const dtkArray& o)

    Copy constructor. Constructs the container with the copy of the
    contents of \a o.

*/

/*! \fn dtkArray::dtkArray(const dtkArray& o, const dtkBufferManager<T>& manager)

    Copy constructor. Constructs the container with the copy of the
    contents of \a o and using a custom dtkBufferManager \a manager.

*/

/*! \fn dtkArray::dtkArray(dtkArray&& o)

    Move constructor. Constructs the container with the contents of \a
    o using move semantics.
*/

/*! \fn dtkArray::dtkArray(dtkArray&& o, const dtkBufferManager<T>& manager)

    Move constructor. Constructs the container with the contents of \a
    o using move semanticsand using a custom dtkBufferManager \a
    manager.
*/

/*! \fn dtkArray::dtkArray(std::initializer_list<T> list, const dtkBufferManager<T>& manager = dtkBufferManager<T>())

    Constructs the container with the contents of the initializer list
    \a list. Optionally, user can supply a dtkBufferManager<T> \a
    manager.
*/

/*! \fn dtkArray::dtkArray(Iterator first, Iterator last, const dtkBufferManager<T>& manager = dtkBufferManager<T>())

    Constructs the container with the contents of the range \c{[}\a
    first, \a last \c{)}. Optionally, user can supply a
    dtkBufferManager<T> \a manager.
*/

/*! \fn dtkArray::dtkArray(const T *buffer, size_type count, const dtkBufferManager<T>& manager = dtkBufferManager<T>())

    Constructs the container by copying \a count elements starting at
    the address \a buffer. Optionally, user can supply a
    dtkBufferManager<T> \a manager.
*/

/*! \fn dtkArray::~dtkArray(void)

    Destructs the container. The destructors of the elements are
    called and the used storage is deallocated. Note, that if the
    elements are pointers, the pointed-to objects are not destroyed.
*/

/*! \fn dtkArray& dtkArray::operator = (const dtkArray& o)

    Copy assignment operator. Replaces the contents with a copy of the
    contents of \a o.

    \code
    #include <dtkArray>
    #include <iostream>

    void display_sizes(const dtkArray<int>& nums1,
                       const dtkArray<int>& nums2,
                       const dtkArray<int>& nums3)
    {
        std::cout <<  "nums1: " << nums1.size()
                  << " nums2: " << nums2.size()
                  << " nums3: " << nums3.size() << '\n';
    }

    int main()
    {
        dtkArray<int> nums1 {3, 1, 4, 6, 5, 9};
        dtkArray<int> nums2;
        dtkArray<int> nums3;

        std::cout << "Initially:\n";
        display_sizes(nums1, nums2, nums3);

        // copy assignment copies data from nums1 to nums2
        nums2 = nums1;

        std::cout << "After assigment:\n";
        display_sizes(nums1, nums2, nums3);

        // move assignment moves data from nums1 to nums3,
        // modifying both nums1 and nums3
        nums3 = std::move(nums1);

        std::cout << "After move assigment:\n";
        display_sizes(nums1, nums2, nums3);
    }
    \endcode

    The above code produces:
    \code
    Initially:
    nums1: 6 nums2: 0 nums3: 0
    After assigment:
    nums1: 6 nums2: 6 nums3: 0
    After move assigment:
    nums1: 0 nums2: 6 nums3: 6
    \endcode
*/

/*! \fn dtkArray& dtkArray::operator = (const dtkArrayView& o)

    Copy assignment operator. Replaces the contents with a copy of the
    contents of a the view \a o.
*/

/*! \fn dtkArray& dtkArray::operator = (const dtkConstArrayView& o)

    Copy assignment operator. Replaces the contents with a copy of the
    contents of a the const view \a o.
*/

/*! \fn dtkArray& dtkArray::operator = (dtkArray&& o)

    Move assignment operator. Replaces the contents with those of
    \a o using move semantics (i.e. the data in other is moved from
    \a o into this container). \a o is in a valid but unspecified
    state afterwards.
*/

/*! \fn dtkArray& dtkArray::operator = (std::initializer_list<T> list)

    Replaces the contents with those identified by initializer list \a
    list.
*/

/*! \fn bool dtkArray::operator == (const dtkArray& o) const

    Returns \c{true} if the content of the container is equal to the
    content of \a o, \c{false} otherwise
*/

/*! \fn bool dtkArray::operator != (const dtkArray& o) const

    Returns \c{false} if the content of the container differs from the
    content of \a o, \c{true} otherwise
*/

/*! \fn bool dtkArray::empty(void) const

    Returns \c true if \c{begin() == end()}, i.e. whether the container
    is empty. Returns \c false otherwise.

    The following code uses empty to check if a dtkArray<int> contains
    any elements:

    \code
    #include <dtkArray>
    #include <iostream>

    int main()
    {
       dtkArray<int> numbers;
       std::cout << "Initially, numbers.empty(): " << numbers.empty() << '\n';

       numbers.push_back(42);
       numbers.push_back(13317);
       std::cout << "After adding elements, numbers.empty(): " << numbers.empty() << '\n';
    }
    \endcode

    It produces:

    \code
    Initially, numbers.empty(): 1
    After adding elements, numbers.empty(): 0
    \endcode
*/

/*! \fn dtkArray::size_type dtkArray::size(void) const

    Returns the number of elements in the container,
    i.e. \c{std::distance(begin(), end())}.

    \code
    #include <dtkArray>
    #include <iostream>

    int main()
    {
       dtkArray<int> nums {1, 3, 5, 7};

       std::cout << "nums contains " << nums.size() << " elements.\n";
    }
    \endcode

    The above code produces:
    \code
    nums contains 4 elements.
    \endcode

    \sa count, length, capacity, resize
*/

/*! \fn dtkArray::size_type dtkArray::capacity(void) const

    Returns the number of elements that the container has currently
    allocated space for.

    \sa size, reserve
*/

/*! \fn dtkArray::size_type dtkArray::stride(void) const

    Returns stride in memory between elements. So \c{array.data() +
    array.stride()} is the address of \c{array[1]}.

    \internal
*/

/*! \fn dtkArray::size_type dtkArray::count(void) const

    Returns the number of elements in the container,
    i.e. \c{std::distance(begin(), end())}.

    \sa size, length, capacity, resize
*/

/*! \fn dtkArray::size_type dtkArray::length(void) const

    Returns the number of elements in the container,
    i.e. \c{std::distance(begin(), end())}.

    \sa size, count, capacity, resize
*/

/*! \fn void dtkArray::swap(dtkArray& o)

    Exchanges the contents of the container with those of \a o. Does
    not invoke any move, copy, or swap operations on individual
    elements.

    \code
    #include <dtkArray>
    #include <iostream>

    int main()
    {
        dtkArray<int> a1{1, 2, 3};
        dtkArray<int> a2{7, 8, 9};

        std::cout << "a1: " << a1;
        std::cout << "\na2: " << a2;

        std::cout << "\n-- SWAP\n";
        a2.swap(a1);

        std::cout << "a1: " << a1;
        std::cout << "\na2: " << a2;
    }
    \endcode

    The above code produces:

    \code
    a1: Array of size 3:
    [ 1, 2, 3 ]
    a2: Array of size 3:
    [ 7, 8, 9 ]
    -- SWAP
    a1: Array of size 3:
    [ 7, 8, 9 ]
    a2: Array of size 3:
    [ 1, 2, 3 ]
    \endcode
*/

/*! \fn void dtkArray::clear(void)

    Removes all elements from the container.

    Invalidates any references, pointers, or iterators referring to
    contained elements. May invalidate any past-the-end iterators.

    Leaves the capacity() of the array unchanged.
*/

/*! \fn void dtkArray::resize(size_type count)

    Resizes the container to contain \a count elements.

    If the current size is greater than \a count, the container is
    reduced to its first \a count elements.

    If the current size is less than \a count, additional
    default-inserted elements are appended.

    \sa reserve
*/

/*! \fn void dtkArray::resize(size_type count, const T& value)

    Resizes the container to contain \a count elements.

    If the current size is greater than \a count, the container is
    reduced to its first \a count elements.

    If the current size is less than \a count, additional copies of
    \a value are appended.

    The following code
    \code
    #include <iostream>
    #include <dtkArray>

    int main()
    {
        dtkArray<int> c = {1, 2, 3};
        std::cout << "The array holds: ";
        for(auto& el: c) std::cout << el << ' ';
        std::cout << '\n';
        c.resize(5, 7);
        std::cout << "After resize up 5 with default value 7: ";
        for(auto& el: c) std::cout << el << ' ';
        std::cout << '\n';
        c.resize(2);
        std::cout << "After resize down to 2: ";
        for(auto& el: c) std::cout << el << ' ';
        std::cout << '\n';
    }
    \endcode

    produces:

    \code
    The array holds: 1 2 3
    After resize up 5 with default value 7: 1 2 3 7 7
    After resize down to 2: 1 2
    \endcode

    \sa reserve
*/

/*! \fn void dtkArray::reserve(size_type new_cap)

    Increases the capacity of the container to a value that's greater
    or equal to \a new_cap. If \a new_cap is greater than the current
    capacity(), new storage is allocated, otherwise the method does
    nothing.

    If \a new_cap is greater than capacity(), all iterators and
    references, including the past-the-end iterator, are
    invalidated. Otherwise, no iterators or references are
    invalidated.

    \code
    #include <iostream>
    #include <dtkArray>

    int main()
    {
        dtkArray<int> a;
        std::cout << "The array has capacity: " << a.capacity() << '\n';
        std::cout << "The array has size:     " << a.size() << '\n';

        a.reserve(11);
        std::cout << "The array has capacity: " << a.capacity() << '\n';
        std::cout << "The array has size:     " << a.size() << '\n';

        a.resize(13);
        std::cout << "The array has capacity: " << a.capacity() << '\n';
        std::cout << "The array has size:     " << a.size() << '\n';
    }
    \endcode

    The above code produces:

    \code
    The array has capacity: 0
    The array has size:     0
    The array has capacity: 11
    The array has size:     0
    The array has capacity: 13
    The array has size:     13
    \endcode
*/

/*! \fn void dtkArray::shrink_to_fit(void)

    Requests the removal of unused capacity.

    All iterators, including the past the end iterator, are
    potentially invalidated.

    \code
    #include <iostream>
    #include <dtkArray>

    int main()
    {
        dtkArray<int> a;
        std::cout << "Default-constructed capacity is " << a.capacity() << '\n';
        a.resize(100);
        std::cout << "Capacity of a 100-element array is " << a.capacity() << '\n';
        a.clear();
        std::cout << "Capacity after clear() is " << a.capacity() << '\n';
        a.shrink_to_fit();
        std::cout << "Capacity after shrink_to_fit() is " << a.capacity() << '\n';
    }
    \endcode

    The above code produces:
    \code
    Default-constructed capacity is 0
    Capacity of a 100-element array is 100
    Capacity after clear() is 100
    Capacity after shrink_to_fit() is 0
    \endcode

    \sa size, capacity, squeeze
*/

/*! \fn void dtkArray::squeeze(void)

    Qt like alias for shrink_to_fit()

    \sa shrink_to_fit
*/

/*! \fn void dtkArray::fill(const T& value)

    Assigns \a value to all items in the array.

    Example:

    \code
    #include <iostream>
    #include <dtkArray>

    int main()
    {
        dtkArray<QString> array(3);
        array.fill("Yes");
        std::cout << array;

        array.fill("oh");
        std::cout << array;
    }
    \endcode

    The above code produces:
    \code
    Array of size 3:
    [ "Yes", "Yes", "Yes" ]
    Array of size 3:
    [ "Oh", "Oh", "Oh" ]
    \endcode
*/

/*! \fn void dtkArray::insert(size_type index, const T& value)

    Inserts \a value at position \a index in the container.

    Causes reallocation if the new size() is greater than the old
    capacity(). If the new size() is greater than capacity(), all
    iterators and references are invalidated. Otherwise, only the
    iterators and references before the insertion point remain
    valid. The past-the-end iterator is also invalidated.

    \sa emplace, push_back

*/

/*! \fn void dtkArray::insert(size_type index, size_type count, const T& value)

    Inserts \a count copies of \a value from position \a index.

    Causes reallocation if the new size() is greater than the old
    capacity(). If the new size() is greater than capacity(), all
    iterators and references are invalidated. Otherwise, only the
    iterators and references before the insertion point remain
    valid. The past-the-end iterator is also invalidated.

    \sa emplace, push_back
*/

/*! \fn iterator dtkArray::insert(const_iterator pos, const T& value)

    Inserts \a value before \a pos.

    Causes reallocation if the new size() is greater than the old
    capacity(). If the new size() is greater than capacity(), all
    iterators and references are invalidated. Otherwise, only the
    iterators and references before the insertion point remain
    valid. The past-the-end iterator is also invalidated.

    \code
    #include <iostream>
    #include <dtkArray>

    int main ()
    {
        dtkArray<int> a(3,100);
        for(auto v : a)
            std::cout << v;
        std::cout << '\n';

        auto it = a.begin();
        it = a.insert(it, 200);
        for(auto v : a)
            std::cout << v;
        std::cout << '\n';

        a.insert(it, 2, 300);
        for(auto v : a)
            std::cout << v;
        std::cout << '\n';

        // "it" no longer valid, get a new one:
        it = a.begin();

        dtkArray<int> a2(2,400);
        a.insert(it+2, a2.begin(), a2.end());
        for(auto v : a)
            std::cout << v;
        std::cout << '\n';

        int arr[] = { 501, 502, 503 };
        vec.insert(a.begin(), arr, arr + 3);
        for(auto v : a)
            std::cout << v;
        std::cout << '\n';
    }
    \endcode

    The above code produces:
    \code
    100 100 100
    200 100 100 100
    300 300 200 100 100 100
    300 300 400 400 200 100 100 100
    501 502 503 300 300 400 400 200 100 100 100
    \endcode

    \sa emplace, push_back
*/

/*! \fn iterator dtkArray::insert(const_iterator pos, T&& value)

    Inserts \a value before \a pos using move semantics.

    Causes reallocation if the new size() is greater than the old
    capacity(). If the new size() is greater than capacity(), all
    iterators and references are invalidated. Otherwise, only the
    iterators and references before the insertion point remain
    valid. The past-the-end iterator is also invalidated.

    \sa emplace, push_back
*/

/*! \fn iterator dtkArray::insert(const_iterator pos, size_type count, const T& value)

    Inserts \a count copies of \a value before \a pos.

    Causes reallocation if the new size() is greater than the old
    capacity(). If the new size() is greater than capacity(), all
    iterators and references are invalidated. Otherwise, only the
    iterators and references before the insertion point remain
    valid. The past-the-end iterator is also invalidated.

    \sa emplace, push_back
*/

/*! \fn iterator dtkArray::insert(const_iterator pos, Iterator first, Iterator last)

    Inserts elements from range \c{[} \a first, \a last \c{)} before \a pos.

    Causes reallocation if the new size() is greater than the old
    capacity(). If the new size() is greater than capacity(), all
    iterators and references are invalidated. Otherwise, only the
    iterators and references before the insertion point remain
    valid. The past-the-end iterator is also invalidated.

    \sa emplace, push_back
*/

/*! \fn iterator dtkArray::insert(const_iterator pos, std::initializer_list<T> list)

     Inserts elements from initializer list \a list before \a pos.

    Causes reallocation if the new size() is greater than the old
    capacity(). If the new size() is greater than capacity(), all
    iterators and references are invalidated. Otherwise, only the
    iterators and references before the insertion point remain
    valid. The past-the-end iterator is also invalidated.

    \sa emplace, push_back
*/

/*! \fn void dtkArray::append(const T& value)

    Appends the given element \a value to the end of the container.

    The new element is initialized as a copy of value.

    If the new size() is greater than capacity() then all iterators
    and references (including the past-the-end iterator) are
    invalidated. Otherwise only the past-the-end iterator is
    invalidated.

    \code
    #include <dtkArray>
    #include <iostream>
    #include <iomanip>

    int main()
    {
        dtkArray<std::string> numbers;

        numbers.append("abc");
        std::string s = "def";
        numbers.append(std::move(s));

        std::cout << "array holds: ";
        for (auto&& i : numbers)
            std::cout << std::quoted(i) << ' ';

        std::cout << "\nMoved-from string holds " << std::quoted(s) << '\n';
     }
     \endcode

     The above code produces:
     \code
     array holds: "abc" "def"
     Moved-from string holds ""
     \endcode

    \sa push_back, emplace_back, pop_back, operator<<, operator+=
*/

/*! \fn void dtkArray::append(T&& value)

    Appends the given element \a value to the end of the container.

    \a value is moved into the new element.

    If the new size() is greater than capacity() then all iterators
    and references (including the past-the-end iterator) are
    invalidated. Otherwise only the past-the-end iterator is
    invalidated.

    \sa push_back, emplace_back, pop_back
*/

/*! \fn void dtkArray::append(const dtkArray<T>& o)

    Appends the elements of array \a o to the end of the container.

    If the new size() is greater than capacity() then all iterators
    and references (including the past-the-end iterator) are
    invalidated. Otherwise only the past-the-end iterator is
    invalidated.

    \code
    #include <dtkArray>
    #include <iostream>
    #include <iomanip>

    int main()
    {
        dtkArray<int> s = { 1, 2, 3, 4, 5, 6 };

        dtkArray<int> t;
        t.append(s);
        std::cout << "t = " << t << std::endl;
     }
     \endcode

     The above code produces:
     \code
     t = Array of size 6:
     [ 1, 2, 3, 4, 5, 6 ]
     \endcode

    \sa push_back, emplace_back, pop_back, operator<<, operator+=
*/

/*! \fn void dtkArray::append(std::initializer_list<T> list)

    Appends the elements of \a list to the end of the container.

    If the new size() is greater than capacity() then all iterators
    and references (including the past-the-end iterator) are
    invalidated. Otherwise only the past-the-end iterator is
    invalidated.

    \code
    #include <dtkArray>
    #include <iostream>

    int main()
    {
        dtkArray<int> t;
        t.append({ 1, 2, 3, 4, 5, 6 });
        std::cout << "t = " << t << std::endl;
     }
     \endcode

     The above code produces:
     \code
     t = Array of size 6:
     [ 1, 2, 3, 4, 5, 6 ]
     \endcode

    \sa push_back, emplace_back, pop_back, operator<<, operator+=
*/

/*! \fn void dtkArray::append(const T *values, size_type length)

    Appends \a length elements from buffer address \a values to the
    end of the container.

    If the new size() is greater than capacity() then all iterators
    and references (including the past-the-end iterator) are
    invalidated. Otherwise only the past-the-end iterator is
    invalidated.

    \code
    #include <dtkArray>
    #include <iostream>

    int main()
    {
        dtkArray<int> s = { 1, 2, 3, 4, 5, 6 };

        dtkArray<int> t;
        t.append(s.data() + 1, 4);
        std::cout << "t = " << t << std::endl;
     }
     \endcode

     The above code produces:
     \code
     t = Array of size 4:
     [ 2, 3, 4, 5 ]
     \endcode

    \sa push_back, emplace_back, pop_back
*/

/*! \fn void dtkArray::prepend(const T& value)

    Prepends the given element \a value to the begining of the
    container.

    If the new size() is greater than capacity() then all iterators
    and references (including the past-the-end iterator) are
    invalidated. Otherwise only the past-the-end iterator is
    invalidated.

    \code
    #include <dtkArray>
    #include <iostream>

    int main()
    {
        dtkArray<int> s = { 1, 2, 3, 4, 5, 6 };
        s.prepend(0);
        std::cout << "s = " << s << std::endl;
     }
     \endcode

     The above code produces:
     \code
     s = Array of size 7:
     [ 0, 1, 2, 3, 4, 5, 6 ]
     \endcode

    \sa insert, emplace, append
*/

/*! \fn void dtkArray::push_back(const T& value)

    Appends the given element \a value to the end of the container.

    The new element is initialized as a copy of value.

    If the new size() is greater than capacity() then all iterators
    and references (including the past-the-end iterator) are
    invalidated. Otherwise only the past-the-end iterator is
    invalidated.

    \code
    #include <dtkArray>
    #include <iostream>
    #include <iomanip>

    int main()
    {
        dtkArray<std::string> numbers;

        numbers.push_back("abc");
        std::string s = "def";
        numbers.push_back(std::move(s));

        std::cout << "array holds: ";
        for (auto&& i : numbers)
            std::cout << std::quoted(i) << ' ';

        std::cout << "\nMoved-from string holds " << std::quoted(s) << '\n';
     }
     \endcode

     The above code produces:
     \code
     array holds: "abc" "def"
     Moved-from string holds ""
     \endcode

     \sa append, emplace_back, pop_back
*/

/*! \fn void dtkArray::push_back(T&& value)

    Appends the given element \a value to the end of the container.

    \a value is moved into the new element.

    If the new size() is greater than capacity() then all iterators
    and references (including the past-the-end iterator) are
    invalidated. Otherwise only the past-the-end iterator is
    invalidated.

    \sa append, emplace_back, pop_back
*/

/*! \fn dtkArray& dtkArray::operator += (const T& value)

    Appends the given element \a value to the end of the container.

    \code
    #include <dtkArray>
    #include <iostream>

    int main()
    {
        dtkArray<std::string> numbers;
        numbers += "abc";
        numbers += "def";
        std::cout << "numbers: " << numbers << std::endl;

     }
     \endcode

     The above code produces:
     \code
     numbers: Array of size 2
     [ "abc", "def" ]
     \endcode

    \sa append, push_back, operator<<
*/

/*! \fn dtkArray& dtkArray::operator += (const dtkArray<T>& o)

    Appends the items of the \a o array to this array and returns a
    reference to this array.

    \code
    #include <dtkArray>
    #include <iostream>

    int main()
    {
        dtkArray<std::string> input = { "abc", "def" };
        dtkArray<std::string> numbers;
        numbers += input;
        std::cout << "numbers: " << numbers << std::endl;

     }
     \endcode

     The above code produces:
     \code
     numbers: Array of size 2
     [ "abc", "def" ]
     \endcode

    \sa append, operator<<
*/

/*! \fn dtkArray& dtkArray::operator += (std::initializer_list<T> list)

    Appends the elements of \a list to the end of the container.

    \code
    #include <dtkArray>
    #include <iostream>

    int main()
    {
        auto list = { "abc", "def" };
        dtkArray<std::string> numbers;
        numbers += list;
        std::cout << "numbers: " << numbers << std::endl;

     }
     \endcode

     The above code produces:
     \code
     numbers: Array of size 2
     [ "abc", "def" ]
     \endcode

    \sa append, operator<<
*/

/*! \fn dtkArray& dtkArray::operator << (const T& value)

    Appends the given element \a value to the end of the container and
    returns a reference to this array.

    \code
    #include <dtkArray>
    #include <iostream>

    int main()
    {
        dtkArray<std::string> numbers;
        numbers << "abc";
        numbers << "def";
        std::cout << "numbers: " << numbers << std::endl;

     }
     \endcode

     The above code produces:
     \code
     numbers: Array of size 2
     [ "abc", "def" ]
     \endcode

    \sa append, push_back, operator+=
*/

/*! \fn dtkArray& dtkArray::operator << (const dtkArray<T>& o)

    Appends the items of the \a o array to this array and returns a
    reference to this array.

    \code
    #include <dtkArray>
    #include <iostream>

    int main()
    {
        dtkArray<std::string> input = { "abc", "def" };
        dtkArray<std::string> numbers;
        numbers << input;
        std::cout << "numbers: " << numbers << std::endl;

     }
     \endcode

     The above code produces:
     \code
     numbers: Array of size 2
     [ "abc", "def" ]
     \endcode

    \sa append, operator+=
*/

/*! \fn dtkArray& dtkArray::operator << (std::initializer_list<T> list)

    Appends the elements of \a list to the end of the container.

    \code
    #include <dtkArray>
    #include <iostream>

    int main()
    {
        auto list = { "abc", "def" };
        dtkArray<std::string> numbers;
        numbers << list;
        std::cout << "numbers: " << numbers << std::endl;

    }
    \endcode

    The above code produces:
    \code
    numbers: Array of size 2
    [ "abc", "def" ]
    \endcode

    \sa append, operator+=
*/

/*! \fn iterator dtkArray::emplace(const_iterator pos, Args&& args)

    Inserts a new element into the container directly before \a
    pos. The element is constructed through
    dtkBufferManager::construct, which typically uses placement-new
    to construct the element in-place at a location provided by the
    container. The arguments \a args... are forwarded to the
    constructor as \c {std::forward<Args>(} \a args \c{)...}.

    If the new size() is greater than capacity(), all iterators and
    references are invalidated. Otherwise, only the iterators and
    references before the insertion point remain valid. The
    past-the-end iterator is also invalidated.

    \code
    #include <dtkArray>
    #include <string>
    #include <iostream>

    struct ComplexValue
    {
        ComplexValue() : m_value(-1), m_mode("Default") {}
        ComplexValue(int value, std::string mode) : m_value(value), m_mode(mode) {}
        ComplexValue(const ComplexValue& other) : m_value(other.m_value), m_mode("Copied") {}
        ComplexValue(ComplexValue&& other) : m_value(std::move(other.m_value)), m_mode("Moved") {}

        ComplexValue& operator = (const ComplexValue& o) = default;

        int value(void) const { return m_value; }
        std::string mode(void) const { return m_mode; }

    private:
        int m_value;
        std::string m_mode;
    };

    std::ostream& operator << (std::ostream& out, const ComplexValue& cv)
    {
        out << "(Complex Value : value = " << std::to_string(cv.value()) << ", mode = " << cv.mode() << ")" ;
        return out;
    }

    int main()
    {
        dtkArray<ComplexValue> a; a.reserve(7);
        a.emplace_back(0, "Init");
        a.emplace_back(1, "Init");

        a.push_back(ComplexValue(3, "Init"));
        a.push_back(ComplexValue(4, "Init"));

        for(const ComplexValue& cv : a)
            std::cout << cv << std::endl;

        std::cout << "---" << std::endl;
        auto cit = a.cbegin() + 2;
        a.emplace(cit, 2, "Init");

        for(const ComplexValue& cv : a)
            std::cout << cv << std::endl;
    }
    \endcode

    The above code produces:
    \code
    (ComplexValue : value = 0, mode = Init)
    (ComplexValue : value = 1, mode = Init)
    (ComplexValue : value = 3, mode = Moved)
    (ComplexValue : value = 4, mode = Moved)
    ---
    (ComplexValue : value = 0, mode = Init)
    (ComplexValue : value = 1, mode = Init)
    (ComplexValue : value = 2, mode = Init)
    (ComplexValue : value = 3, mode = Moved)
    (ComplexValue : value = 4, mode = Copied)
    \endcode

    Here, the last element is copy constructed on a non-initialized
    memory.

    \sa insert
*/

/*! \fn void dtkArray::emplace_back(Args&& args)

    Appends a new element to the end of the container. The element is
    constructed through dtkBufferManager::construct, which
    typically uses placement-new to construct the element in-place at
    the location provided by the container. The arguments \a args... are
    forwarded to the constructor as \c {std::forward<Args>(} \a args \c{)...}.

    If the new size() is greater than capacity() then all iterators
    and references (including the past-the-end iterator) are
    invalidated. Otherwise only the past-the-end iterator is
    invalidated.

    \sa push_back, emplace, append
*/

/*! \fn iterator dtkArray::erase(const_iterator pos)

    Removes the element at \a pos.

    Returns the iterator following the last removed element. If the
    iterator pos refers to the last element, the end() iterator is
    returned.

    Invalidates iterators and references at or after the point of the
    erase, including the end() iterator.

    The const iterator \a pos must be valid and dereferenceable. Thus
    the end() iterator (which is valid, but is not dereferencable)
    cannot be used as a value for \a pos.

    \code
    #include <dtkArray>
    #include <iostream>

    int main( )
    {
        dtkArray<int> c {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        for (auto &i : c) {
            std::cout << i << " ";
        }
        std::cout << '\n';

        c.erase(c.begin());

        for (auto &i : c) {
            std::cout << i << " ";
        }
        std::cout << '\n';

        c.erase(c.begin()+2, c.begin()+5);

        for (auto &i : c) {
            std::cout << i << " ";
        }
        std::cout << '\n';
    }
    \endcode
    The above code produces:
    \code
    0 1 2 3 4 5 6 7 8 9
    1 2 3 4 5 6 7 8 9
    1 2 6 7 8 9
    \endcode

    \sa clear, remove
*/

/*! \fn iterator dtkArray::erase(const_iterator first, const_iterator last)

    Removes the elements in the range \c{[} \a first \c{;} \a last \c{)}.

    Returns the iterator following the last removed element. If the
    iterator pos refers to the last element, the end() iterator is
    returned.

    Invalidates iterators and references at or after the point of the
    erase, including the end() iterator.

    The iterator first does not need to be dereferenceable if
    \a first \c{==} \a last: erasing an empty range is a no-op.

    \sa clear, remove
*/

/*! \fn void dtkArray::pop_back(void)

    Removes the last element of the container.

    Calling pop_back on an empty container is undefined.

    No iterators or references except for back() and end() are
    invalidated.

    \sa push_back
*/

/*! \fn void dtkArray::remove(size_type index)

    Removes the element at position \a index.

    \code
    #include <dtkArray>
    #include <iostream>

    int main( )
    {
        dtkArray<int> c {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        for (auto &i : c) {
            std::cout << i << " ";
        }
        std::cout << '\n';

        c.remove(0);

        for (auto &i : c) {
            std::cout << i << " ";
        }
        std::cout << '\n';

        c.remove(2, 3;

        for (auto &i : c) {
            std::cout << i << " ";
        }
        std::cout << '\n';
    }
    \endcode
    The above code produces:
    \code
    0 1 2 3 4 5 6 7 8 9
    1 2 3 4 5 6 7 8 9
    1 2 6 7 8 9
    \endcode

    \sa erase, clear
*/

/*! \fn void dtkArray::remove(size_type index, size_type count)

    Removes \a count elements from the middle of the vector, starting
    at position \a index.

    \sa erase, clear
*/

/*! \fn void dtkArray::removeFirst(void)

    Removes the first item in the array. Calling this function is
    equivalent to calling remove(0). The array must not be empty. If
    the array can be empty, call empty() before calling this
    function.

    \sa remove, takeFirst, empty
*/

/*! \fn void  dtkArray::removeLast(void)

    Removes the last item in the array. Calling this function is
    equivalent to calling remove(size() - 1). The array must not be
    empty. If the array can be empty, call empty() before calling
    this function.

    \sa remove, takeLast, empty
*/

/*! \fn value_dtkArray::type takeFirst(void)

    Removes the first item in the array and returns it. This function
    assumes the array is not empty. To avoid failure, call empty()
    before calling this function.

    \sa takeAt, takeLast, removeFirst
*/

/*! \fn value_type dtkArray::takeLast(void)

    Removes the last item in the list and returns it. This function
    assumes the array is not empty. To avoid failure, call empty()
    before calling this function.

    If you don't use the return value, removeLast() is more efficient.

    \sa takeAt, takeFirst, removeLast
*/

/*! \fn value_type dtkArray::takeAt(size_type index)

    Removes the element at position \a index and returns it.

    Equivalent to
    \code
    T t = at(index);
    remove(index);
    return t;
    \endcode

    \sa takeFirst, takeLast
*/

/*! \fn const_reference dtkArray::at(size_type index) const

    Returns a reference to the element at specified location \a index,
    with bounds checking in debug mode.

    \a index must correspond to a valid position in the array (i.e., \c {0
    <= index < size()} ).

    \sa operator[]
*/

/*! \fn const_reference dtkArray::front(void) const

    Returns a reference to the first element in the container.

    Calling front on an empty container is undefined.

    \code
    #include <dtkArray>
    #include <iostream>

    int main()
    {
         dtkArray<char> letters {'o', 'm', 'g', 'w', 't', 'f'};

         if (!letters.empty()) {
            std::cout << "The first character is: " << letters.front() << '\n';
         }
     }
     \endcode
     The above code produces :
     \code
     The first character is o
     \endcode

    \sa first, back
*/

/*! \fn reference dtkArray::front(void)

    Returns a reference to the first element in the container.

    Calling front on an empty container is undefined.

    \sa first, back
*/

/*! \fn const_reference dtkArray::back(void) const

    Returns reference to the last element in the container.

    Calling back on an empty container is undefined.

    \code
    #include <dtkArray>
    #include <iostream>

    int main()
    {
         dtkArray<char> letters {'o', 'm', 'g', 'w', 't', 'f'};

         if (!letters.empty()) {
            std::cout << "The last character is: " << letters.back() << '\n';
         }
     }
     \endcode
     The above code produces :
     \code
     The last character is f
     \endcode

    \sa last, front
*/

/*! \fn reference dtkArray::back(void)

    Returns reference to the last element in the container.

    Calling back on an empty container is undefined.

    \sa last, front
*/

/*! \fn const_reference dtkArray::first(void) const

    Returns a reference to the first item in the array. This function
    assumes that the array isn't empty.

    \sa front, last
*/

/*! \fn reference dtkArray::first(void)

    Returns a reference to the first item in the array. This function
    assumes that the array isn't empty.

    \sa front, last
*/

/*! \fn const_reference dtkArray::last(void) const

    Returns a reference to the last item in the array. This function
    assumes that the array isn't empty.

    \sa first, back
*/

/*! \fn reference dtkArray::last(void)

    Returns a reference to the last item in the array. This function
    assumes that the array isn't empty.

    \sa first, back
*/

/*! \fn const_reference dtkArray::operator[](size_type index) const

    Returns a const reference to the element at specified location \a index.

    No bounds checking is performed.

    \code
    #include <dtkArray.h>
    #include <iostream>

    int main()
    {
        dtkArray<int> numbers {2, 4, 6, 8};

        std::cout << "Second element: " << numbers[1] << '\n';

        numbers[0] = 5;

        std::cout << "All numbers:";
        for (auto i : numbers) {
            std::cout << ' ' << i;
        }
        std::cout << '\n';
    }
    \endcode
    The above code produces:
    \code
    Second element: 4
    All numbers: 5 4 6 8
    \endcode

    \sa at, setAt
*/

/*! \fn reference dtkArray::operator[](size_type index)

    Returns a reference to the element at specified location \a index.

    No bounds checking is performed.

    \sa at, setAt
*/

/*! \fn void dtkArray::setAt(size_type index, const T& value)

    Replaces the item at position \a index with \a value.

    \a index must reference a valid position (\e {i.e.} \a index \c {<} size()).\code
    #include <dtkArray.h>
    #include <iostream>

    int main()
    {
        dtkArray<double> numbers {0.2, 0.4, 0.6, 0.8};

        numbers.setAt(0, 0.5);

        double val[3] = {0.3, 0.9, 2.7};

        numbers.setAt(1, val, 3);

        std::cout << "All numbers:";
        for (auto i : numbers) {
            std::cout << ' ' << i;
        }
        std::cout << '\n';
    }
    \endcode
    The above code produces:
    \code
    All numbers: 0.5 0.3 0.9 2.7
    \endcode

    \sa operator[], remove
*/

/*! \fn void dtkArray::setAt(size_type index, const T *values, size_type length)

    Replaces the items of the range \c {[} \a index \c{,} \a index
    \c{+} \a length \c{]} with the values of the buffer \a values

    \a index must reference a valid position (\e {i.e.} \a index \c {<} size()).

    \a index \c {+} \a length must be lower than size().

    Buffer \a values must contain at least \a length elements.

    \sa insert, operator+=, operator<<
*/


/*! \fn iterator dtkArray::begin(void)

    Returns an iterator to the first element of the container.

    If the container is empty, the returned iterator will be equal to
    end().

    \sa cbegin, end, cend
*/

/*! \fn const_iterator dtkArray::begin(void) const

    Returns a const iterator to the first element of the container.

    If the container is empty, the returned iterator will be equal to
    end().

    \sa cbegin, end, cend
*/

/*! \fn const_iterator dtkArray::cbegin(void) const

    Returns a const iterator to the first element of the container.

    If the container is empty, the returned iterator will be equal to
    end().

    \sa begin, end, cend
*/

/*! \fn iterator dtkArray::end(void)

    Returns an iterator to the element following the last element of
    the container.

    This element acts as a placeholder; attempting to access it
    results in undefined behavior.

    \sa cbegin, begin, cend
*/

/*! \fn const_iterator dtkArray::end(void) const

    Returns a const iterator to the element following the last element
    of the container.

    This element acts as a placeholder; attempting to access it
    results in undefined behavior.

    \sa cbegin, begin, cend

*/

/*! \fn const_iterator dtkArray::cend(void) const

    Returns a const iterator to the element following the last element
    of the container.

    This element acts as a placeholder; attempting to access it
    results in undefined behavior.

    \sa cbegin, begin, end
*/

/*! \fn reverse_iterator dtkArray::rbegin(void)

    Returns a reverse iterator to the first element of the reversed
    container. It corresponds to the last element of the non-reversed
    container.

    \sa crbegin, rend, crend

*/

/*! \fn const_reverse_iterator dtkArray::rbegin(void) const

    Returns a const reverse iterator to the first element of the
    reversed container. It corresponds to the last element of the
    non-reversed container.

    \sa crbegin, rend, crend
*/

/*! \fn const_reverse_iterator dtkArray::crbegin(void) const

    Returns a const reverse iterator to the first element of the
    reversed container. It corresponds to the last element of the
    non-reversed container.

    \sa rbegin, rend, crend
*/

/*! \fn reverse_iterator dtkArray::rend(void)

    Returns a reverse iterator to the element following the last
    element of the reversed container. It corresponds to the element
    preceding the first element of the non-reversed container. This
    element acts as a placeholder, attempting to access it results in
    undefined behavior.

    \sa rbegin, crbegin, crend
*/

/*! \fn const_reverse_iterator dtkArray::rend(void) const

    Returns a const reverse iterator to the element following the last
    element of the reversed container. It corresponds to the element
    preceding the first element of the non-reversed container. This
    element acts as a placeholder, attempting to access it results in
    undefined behavior.

    \sa rbegin, crbegin, crend
*/

/*! \fn const_reverse_iterator dtkArray::crend(void) const

    Returns a const reverse iterator to the element following the last
    element of the reversed container. It corresponds to the element
    preceding the first element of the non-reversed container. This
    element acts as a placeholder, attempting to access it results in
    undefined behavior.

    \sa rbegin, crbegin, rend
*/

/*! \fn const_pointer dtkArray::data(void) const

    Returns a const pointer to the underlying array serving as element
    storage. The pointer is such that range \c{[} data() \c{;} data()
    \c{+} size() \c{)} is always a valid range, even if the container
    is empty (data() is not dereferenceable in that case).

    \sa constData, front, back
*/

/*! \fn pointer dtkArray::data(void)

    Returns a pointer to the underlying array serving as element
    storage. The pointer is such that range \c{[} data() \c{;} data()
    \c{+} size() \c{)} is always a valid range, even if the container
    is empty (data() is not dereferenceable in that case).

    \sa constData, front, back
*/

/*! \fn const_pointer dtkArray::constData(void) const

    Returns a const pointer to the underlying array serving as element
    storage. The pointer is such that range \c{[} data() \c{;} data()
    \c{+} size() \c{)} is always a valid range, even if the container
    is empty (data() is not dereferenceable in that case).

    \sa data, front, back
*/

/*! \fn bool dtkArray::startsWith(const T& value) const

    Returns true if this array is not empty and its first item is
    equal to \a value; otherwise returns false.

    \sa empty, first, endWith
*/

/*! \fn bool dtkArray::endsWith(const T& value) const

    Returns true if this array is not empty and its last item is equal
    to \a value; otherwise returns false.

    \sa empty, last, startWith
*/

/*! \fn bool dtkArray::contains(const T& value) const

    Returns true if the array contains an occurrence of \a value;
    otherwise returns false.

    This function requires the value type to have an implementation of
    operator==().

    \sa indexOf, count
*/

/*! \fn size_type dtkArray::indexOf(const T& value) const

    Returns the index position of the first occurrence of \a value in
    the container. Returns -1 if no item matched.

    Example:
    \code
    dtkArray<std::string> array;
    array << "A" << "B" << "C" << "B" << "A";
    array.indexOf("B");            // returns 1
    array.indexOf("B", 1);         // returns 1
    array.indexOf("B", 2);         // returns 3
    array.indexOf("X");            // returns -1
    \encode

    This function requires the value type to have an implementation of operator==().

    \sa lastIndexOf, contains
*/

/*! \fn size_type dtkArray::indexOf(const T& value, size_type from) const

    Returns the index position of the first occurrence of \a value in
    the container, searching forward from index position \a
    from. Returns -1 if no item matched.

    \sa lastIndexOf, contains
*/

/*! \fn size_type dtkArray::lastIndexOf(const T& value) const

    Returns the index position of the last occurrence of the value \a
    value in the container. The search starts at the last
    item. Returns -1 if no item matched.

    Example:
    \code
    dtkArray<std::string> array;
    array << "A" << "B" << "C" << "B" << "A";
    array.lastIndexOf("B");            // returns 3
    array.lastIndexOf("B", 1);         // returns 3
    array.lastIndexOf("B", 2);         // returns 1
    array.lastIndexOf("X");            // returns -1
    \encode

    This function requires the value type to have an implementation of
    operator==().

    \sa lastIndexOf, contains
*/

/*! \fn size_type dtkArray::lastIndexOf(const T& value, size_type from) const

    Returns the index position of the last occurrence of the value \a
    value in the container, searching backward from index position
    \a from. Returns -1 if no item matched.

    This function requires the value type to have an implementation of
    operator==().

    \sa lastIndexOf, contains
*/

/*! \fn size_type dtkArray::count(const T& value) const

    Returns the number of occurrences of \a value in the container.

    This function requires the value type to have an implementation of
    operator==().

    \sa contains, indexOf
*/

/*! \fn template < typename U > typename std::enable_if<std::is_scalar<U>::value && std::is_convertible<U, T>::value>::type dtkArray::setRawData(U *buffer, size_type count, const dtkBufferManager<U>& manager = dtkBufferManager<U>())

     Resets the dtkArray to use the first \a count elements of the \a
     buffer array. The dtkArray will contain the \a buffer
     pointer.

     According to the type of the supplied BufferManager \a manager,
     the array is responsible for deleting the \a buffer.

*/

/*! \fn void dtkArray::extractRawData(T *& buffer, size_type& count, dtkBufferManager<T>& manager)



*/

/*! \fn void dtkArray::setStaticRawData(T *buffer, size_type count)



*/

/*! \fn ConstView dtkArray::constView(size_type from, size_type to, size_type stride = 1) const



*/

/*! \fn ConstView dtkArray::view(size_type from, size_type to, size_type stride = 1) const



*/

/*! \fn View dtkArray::view(size_type from, size_type to, size_type stride = 1)



*/

//
// dtkArray.cpp ends here
