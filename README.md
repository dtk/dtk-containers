# dtk-containers
This layer aims at providing containers that suit with scientific applications.

### Definition of several concepts

1. bufferManager acting as allocator
2. memoryResource interface with different implementations
  * null
  * new/delete
  * malloc/free
  * align (enabling to alloc memory with a custom alignment)
3. iterator for array
4. array hierarchy enabling the following concepts:
  * ConstArrayView acting as a read-only wrapper around a buffer
  * ArrayView acting as a read-write wrapper around a buffer
  * Array providing both stl-like and Qt-like API for a dynamic array
5. static array with aligned memory
